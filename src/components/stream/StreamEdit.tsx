import _ from "lodash";
import React from "react";
import { connect, ConnectedProps } from "react-redux";
import { createStream, editStream, fetchStream } from "../../actions/streams";
import { RouteComponentProps } from "react-router-dom";
import { Stream, StreamFormProps } from "../../reducers/streamsReducer";
import StreamForm from "./StreamForm";
import { ApplicationState } from "../../reducers";
import { IdMatchParams, strToInt } from "../../utils/common";

export interface StreamEditDispatchProps {
  fetchStream: (id: number) => void;
  deleteStream: (id: number) => void;
}

export interface StreamEditMainProps {
  stream: Stream | null;
}

interface StreamEditState {
  id: number | null;
}

class StreamEdit extends React.Component<StreamEditProps, StreamEditState> {
  constructor(props: StreamEditProps, state: StreamEditState) {
    super(props, state);
    const id = strToInt(this.props.match.params.id);
    this.state = { id: id };
  }
  componentDidMount() {
    if (this.state.id != null) this.props.fetchStream(this.state.id);
  }

  onSubmit = (formValues: StreamFormProps) => {
    const id = this.state.id;
    if (id != null) this.props.editStream(id, formValues);
  };

  render() {
    if (!this.props.stream) {
      return <div>Loading..</div>;
    }
    return (
      <div>
        <h3>Edit a Stream</h3>
        <StreamForm
          initialValues={_.pick(this.props.stream, "title", "description")}
          onSubmit={this.onSubmit}
        />
      </div>
    );
  }
}
const mapStateToProps = (
  state: ApplicationState,
  ownProps: StreamEditMainProps &
    StreamEditDispatchProps &
    RouteComponentProps<IdMatchParams>
) => {
  const id = strToInt(ownProps.match.params.id);
  if (id != null) return { stream: state.streams.items[id] };
  else return null;
};

const connector = connect(mapStateToProps, { fetchStream, editStream });
type StreamEditProps = ConnectedProps<typeof connector> &
  StreamEditMainProps &
  StreamEditDispatchProps &
  RouteComponentProps<IdMatchParams>;

export default connector(StreamEdit);
