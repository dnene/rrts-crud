import React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { RouteComponentProps } from "react-router";
import { Stream, StreamsState } from "../../reducers/streamsReducer";
import { deleteStream, fetchStream, listStreams } from "../../actions/streams";
import Modal from "../Modal";
import { ApplicationState } from "../../reducers";
import { IdMatchParams, strToInt } from "../../utils/common";

export interface StreamDeleteDispatchProps {
  fetchStream: (id: number) => void;
  deleteStream: (id: number) => void;
}

export interface StreamDeleteMainProps {
  stream: Stream | null;
}

interface StreamDeleteState {
  id: number | null;
}
class StreamDelete extends React.Component<
  StreamDeleteProps,
  StreamDeleteState
> {
  constructor(props: StreamDeleteProps, state: StreamDeleteState) {
    super(props, state);
    const id = strToInt(this.props.match.params.id);
    this.state = { id: id };
  }
  componentDidMount() {
    if (this.state.id != null) this.props.fetchStream(this.state.id);
  }

  renderActions() {
    const id = this.state.id;
    if (id != null) {
      return (
        <React.Fragment>
          <button
            onClick={() => this.props.deleteStream(id)}
            className="ui button negative"
          >
            Delete
          </button>
          <Link to="/" className="ui button">
            Cancel
          </Link>
        </React.Fragment>
      );
    } else {
      // todo
      return <div>Invalid Id passed</div>;
    }
  }

  renderContent() {
    if (!this.props.stream) {
      return "Are you sure you want to delete this stream?";
    }
    return `Are you sure you want to delete this stream : ${this.props.stream.title} ?`;
  }
  render() {
    const id = this.state.id;
    if (this.props.stream) {
      return (
        <Modal
          title="Delete Stream"
          content={this.renderContent()}
          actions={this.renderActions()}
          onDismiss={() => {
            this.props.history.push("/");
          }}
        />
      );
    } else {
      return (
        <div>{`No stream found for id ${this.props.match.params.id}`}</div>
      );
    }
  }
}

const mapStateToProps = (
  state: ApplicationState,
  ownProps: StreamDeleteMainProps &
    StreamDeleteDispatchProps &
    RouteComponentProps<IdMatchParams>
) => {
  const id = strToInt(ownProps.match.params.id);
  if (id != null) return { stream: state.streams.items[id] };
  else return null;
};

const connector = connect(mapStateToProps, { fetchStream, deleteStream });
type StreamDeleteProps = ConnectedProps<typeof connector> &
  StreamDeleteMainProps &
  StreamDeleteDispatchProps &
  RouteComponentProps<IdMatchParams>;

export default connector(StreamDelete);
