import React from "react";
import { Stream } from "../../reducers/streamsReducer";
import { IdMatchParams, strToInt } from "../../utils/common";
import { ApplicationState } from "../../reducers";
import { RouteComponentProps } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import { fetchStream } from "../../actions/streams";

export interface StreamShowDispatchProps {
  fetchStream: (id: number) => void;
}

export interface StreamShowMainProps {
  stream: Stream | null;
}

interface StreamShowState {
  id: number | null;
}

class StreamShow extends React.Component<StreamShowProps, StreamShowState> {
  constructor(props: StreamShowProps, state: StreamShowState) {
    super(props, state);
    const id = strToInt(this.props.match.params.id);
    this.state = { id: id };
  }
  componentDidMount() {
    if (this.state.id != null) this.props.fetchStream(this.state.id);
  }
  render() {
    if (!this.props.stream) {
      return <div>Loading ...</div>;
    }
    const { title, description } = this.props.stream;
    return (
      <div>
        <h1>{title}</h1>
        <h5>{description}</h5>
      </div>
    );
  }
}
const mapStateToProps = (
  state: ApplicationState,
  ownProps: StreamShowMainProps &
    StreamShowDispatchProps &
    RouteComponentProps<IdMatchParams>
) => {
  const id = strToInt(ownProps.match.params.id);
  if (id != null) return { stream: state.streams.items[id] };
  else return null;
};

const connector = connect(mapStateToProps, { fetchStream });
type StreamShowProps = ConnectedProps<typeof connector> &
  StreamShowMainProps &
  StreamShowDispatchProps &
  RouteComponentProps<IdMatchParams>;

export default connector(StreamShow);
