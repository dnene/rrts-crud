import React from "react";
import { createStream } from "../../actions/streams";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import { StreamFormProps } from "../../reducers/streamsReducer";
import StreamForm, { StreamDispatchProps } from "./StreamForm";

type StreamCreateRouteParams = {};
class StreamCreate extends React.Component<StreamCreateProps> {
  onSubmit = (formValues: StreamFormProps) => {
    this.props.createStream(formValues);
  };

  render() {
    const dispatchProps: StreamDispatchProps = { onSubmit: this.onSubmit };
    return (
      <div>
        <h3>Create a Stream</h3>
        <StreamForm {...dispatchProps} />
      </div>
    );
  }
}
const connector = connect(null, { createStream });
type StreamCreateProps = ConnectedProps<typeof connector> &
  RouteComponentProps &
  StreamCreateRouteParams;

export default connector(StreamCreate);
