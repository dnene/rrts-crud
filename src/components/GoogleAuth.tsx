import React from "react";
import { signIn, signOut } from "../actions/auth";
import { ApplicationState } from "../reducers";
import { connect } from "react-redux";

interface GoogleAuthProps {
  isSignedIn: boolean | null;
  signIn: typeof signIn;
  signOut: typeof signOut;
}

class GoogleAuth extends React.Component<GoogleAuthProps> {
  auth?: gapi.auth2.GoogleAuth;
  componentDidMount() {
    window.gapi.load("client:auth2", () => {
      window.gapi.client
        .init({
          clientId:
            "1066496831865-59iueum8al7uq3c8963v9h93figh87a2.apps.googleusercontent.com",
          scope: "email",
        })
        .then(() => {
          this.auth = window.gapi.auth2.getAuthInstance();
          this.onAuthChange(this.auth.isSignedIn.get());
          this.auth.isSignedIn.listen(this.onAuthChange);
        });
    });
  }

  onAuthChange = (isSignedIn: boolean) => {
    if (this.auth && isSignedIn) {
      this.props.signIn(this.auth.currentUser.get().getId());
    } else {
      this.props.signOut();
    }
  };

  onSignInClick = () => {
    this.auth?.signIn();
  };

  onSignOutClick = () => {
    this.auth?.signOut();
  };

  renderAuthButton() {
    switch (this.props.isSignedIn) {
      case null:
        return null;
      case true:
        return (
          <button
            className="ui red google button"
            onClick={this.onSignOutClick}
          >
            <i className="google icon"></i>
            Sign Out
          </button>
        );
      case false:
        return (
          <div>
            <button
              className="ui red google button"
              onClick={this.onSignInClick}
            >
              <i className="google icon"></i>
              Sign In with Google
            </button>
          </div>
        );
      default:
        return <div>Something went wrong</div>;
    }
  }

  render() {
    return <div>{this.renderAuthButton()}</div>;
  }
}

const mapStateToProps = (state: ApplicationState) => {
  return { isSignedIn: state.auth.isSignedIn };
};
export default connect(mapStateToProps, { signIn, signOut })(GoogleAuth);
