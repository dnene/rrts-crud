import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  RouteComponentProps,
} from "react-router-dom";

import Header from "./Header";
import StreamList from "./stream/StreamList";
import StreamCreate from "./stream/StreamCreate";
import StreamEdit from "./stream/StreamEdit";
import StreamDelete, {
  StreamDeleteDispatchProps,
  StreamDeleteMainProps,
} from "./stream/StreamDelete";
import StreamShow from "./stream/StreamShow";
import { ApplicationState } from "../reducers";
import { Redirect } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import { deleteStream, fetchStream } from "../actions/streams";
import { IdMatchParams } from "../utils/common";

interface AppMainProps {
  redirectTo: string | null;
}

class App extends React.Component<AppProps> {
  renderRedirect() {
    if (
      this.props &&
      this.props.redirectTo != null &&
      this.props.redirectTo !== window.location.pathname
    ) {
      this.forceUpdate();
      return (
        <Router>
          <Redirect to={this.props.redirectTo} />
        </Router>
      );
    } else {
      return null;
    }
  }
  render() {
    return (
      this.renderRedirect() || (
        <div className="ui container">
          <Router>
            <Header />
            <Switch>
              <Route path="/" exact component={StreamList} />
              <Route path="/streams" exact component={StreamList} />
              <Route path="/streams/new" exact component={StreamCreate} />
              <Route path="/streams/edit/:id" exact component={StreamEdit} />
              <Route
                path="/streams/delete/:id"
                exact
                component={StreamDelete}
              />
              <Route path="/streams/show/:id" exact component={StreamShow} />
            </Switch>
          </Router>
        </div>
      )
    );
  }
}

const mapStateToProps = (state: ApplicationState, ownProps: AppMainProps) => {
  const newProps = { redirectTo: state.streams.redirectTo };
  return newProps;
};

const connector = connect(mapStateToProps, {});
type AppProps = ConnectedProps<typeof connector> & AppMainProps;

export default connector(App);
