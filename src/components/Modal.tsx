import React from "react";
import ReactDOM from "react-dom";

interface ModalProps {
  title: string;
  content: string;
  actions: JSX.Element;
}

interface ModalDispatchProps {
  onDismiss: () => void;
}

class Modal extends React.Component<ModalProps & ModalDispatchProps> {
  render() {
    const element = document.querySelector("#modal");
    if (element != null) {
      return ReactDOM.createPortal(
        <div
          onClick={this.props.onDismiss}
          className="ui dimmer modals visible active"
        >
          <div
            onClick={(e) => e.stopPropagation()}
            className="ui standard modal visible active"
          >
            <div className="header">{this.props.title}</div>
            <div className="content">{this.props.content}</div>
            <div className="actions">{this.props.actions}</div>
          </div>
        </div>,
        element
      );
    } else {
      return <div>Error in generating a modal</div>;
    }
  }
}

export default Modal;
