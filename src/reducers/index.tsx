import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import authReducer from "./authReducer";
import { AuthState } from "./authReducer";
import streamsReducer, { Streams, StreamsState } from "./streamsReducer";

export interface ApplicationState {
  auth: AuthState;
  streams: StreamsState;
}

export default combineReducers({
  form: formReducer,
  auth: authReducer,
  streams: streamsReducer,
});
