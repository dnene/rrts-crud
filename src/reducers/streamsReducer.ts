import _ from "lodash";
import { AllStreamActions, StreamActionTypes } from "../actions/streams";

export interface Stream {
  id: number;
  title: string;
  description: string;
  userId: string;
}

export interface StreamFormProps {
  title: string | null;
  description: string | null;
}

export interface Streams {
  [id: number]: Stream;
}

export interface StreamsState {
  items: Streams;
  loading: boolean;
  error: String | null;
  redirectTo: string | null;
}

const InitialState = {
  items: {},
  loading: true,
  error: null,
  redirectTo: null,
};

export default (
  state: StreamsState = InitialState,
  action: AllStreamActions
) => {
  switch (action.type) {
    case StreamActionTypes.List:
      return {
        items: { ...state.items, ..._.mapKeys(action.payload, "id") },
        loading: false,
        error: null,
        redirectTo: action.redirectTo,
      };
    case StreamActionTypes.Add:
    case StreamActionTypes.Edit:
    case StreamActionTypes.Fetch:
      const act = {
        items: { ...state.items, [action.payload.id]: action.payload },
        loading: false,
        error: null,
        redirectTo: action.redirectTo,
      };
      return act;
    case StreamActionTypes.Delete:
      console.log(`Delete triggered for id ${action.payload}`);
      console.log(action.payload);
      console.log(`state has ${Object.keys(state.items).length} keys`);
      const newItems = _.omit(state.items, action.payload);
      console.log(`state has ${Object.keys(newItems).length} keys`);
      return {
        items: newItems,
        loading: false,
        error: null,
        redirectTo: action.redirectTo,
      };
    case StreamActionTypes.ListFailure:
    case StreamActionTypes.AddFailure:
      return {
        ...state,
        loading: false,
        error: action.payload,
        redirectTo: action.redirectTo,
      };
    default:
      return state;
  }
};
