export enum AuthActions {
  SignIn = 100,
  SignOut,
}

export interface SignInAction {
  type: AuthActions.SignIn;
  payload: number;
}

export interface SignOutAction {
  type: AuthActions.SignOut;
}

export type AllAuthActions = SignInAction | SignOutAction;

export const signIn = (userId: string) => {
  return {
    type: AuthActions.SignIn,
    payload: userId,
  };
};

export const signOut = () => {
  return {
    type: AuthActions.SignOut,
  };
};
