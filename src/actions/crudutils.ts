import { Action } from "redux";
import { AxiosResponse } from "axios";
import { ThunkDispatch } from "redux-thunk";
import { Fault } from "./core";

export interface BaseAction<T, D> extends Action<T> {
  type: T;
  payload: D;
  redirectTo: string | null;
}

export function handleResponse<T, ET, D, S, E>(
  response: AxiosResponse<D>,
  dispatch: ThunkDispatch<S, E, BaseAction<T, D | Fault>>,
  type: T,
  errorType: ET,
  message: string,
  redirectOnSuccessTo: string | null,
  alternatePayload: D | null = null
): void {
  try {
    if (response.status >= 200 && response.status <= 299) {
      const action = {
        type: type,
        payload: alternatePayload ? alternatePayload : response.data,
        redirectTo: redirectOnSuccessTo,
      };
      dispatch(action);
    } else {
      dispatch({
        type: type,
        payload: {
          message: message,
          error: { http_status: response.status.toString() },
        },
        redirectTo: null,
      });
    }
  } catch (e) {
    dispatch({
      type: type,
      payload: {
        message: message,
        error: e,
      },
      redirectTo: null,
    });
  }
}
