export interface Fault {
  message: string;
  error: { [arg: string]: string } | Error | null;
}
