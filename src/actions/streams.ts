import backend from "../apis/backend";
import {
  Stream,
  StreamFormProps,
  StreamsState,
} from "../reducers/streamsReducer";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { AxiosResponse } from "axios";

import { Fault } from "./core";
import { ApplicationState } from "../reducers";
import { BaseAction, handleResponse } from "./crudutils";

export enum StreamActionTypes {
  List = 200,
  Add,
  Fetch,
  Edit,
  Delete,
  ListFailure,
  AddFailure,
  FetchFailure,
  EditFailure,
  DeleteFailure,
}

type StreamListAction = BaseAction<StreamActionTypes.List, Stream[]>;
type StreamAddAction = BaseAction<StreamActionTypes.Add, Stream>;
type StreamFetchAction = BaseAction<StreamActionTypes.Fetch, Stream>;
type StreamEditAction = BaseAction<StreamActionTypes.Edit, Stream>;
type StreamDeleteAction = BaseAction<StreamActionTypes.Delete, number>;

type StreamListFailureAction = BaseAction<StreamActionTypes.ListFailure, Fault>;
type StreamAddFailureAction = BaseAction<StreamActionTypes.AddFailure, Fault>;
type StreamFetchFailureAction = BaseAction<
  StreamActionTypes.FetchFailure,
  Fault
>;
type StreamEditFailureAction = BaseAction<StreamActionTypes.EditFailure, Fault>;
type StreamDeleteFailureAction = BaseAction<
  StreamActionTypes.DeleteFailure,
  Fault
>;

export type AllStreamActions =
  | StreamListAction
  | StreamAddAction
  | StreamFetchAction
  | StreamEditAction
  | StreamDeleteAction
  | StreamListFailureAction
  | StreamAddFailureAction
  | StreamFetchFailureAction
  | StreamEditFailureAction
  | StreamDeleteFailureAction;

export const listStreams = (): ThunkAction<
  void,
  StreamsState,
  undefined,
  StreamListAction
> => async (dispatch) => {
  handleResponse(
    await backend.get("/streams"),
    dispatch,
    StreamActionTypes.List,
    StreamActionTypes.ListFailure,
    "Could not retrieve stream list",
    null
  );
};

export const createStream = (
  formValues: StreamFormProps
): ThunkAction<void, ApplicationState, undefined, StreamAddAction> => async (
  dispatch,
  getState
) => {
  const { userId } = getState().auth;
  handleResponse(
    await backend.post("/streams", { ...formValues, userId: userId }),
    dispatch,
    StreamActionTypes.Add,
    StreamActionTypes.AddFailure,
    "Could not add stream",
    "/streams"
  );
};

export const editStream = (
  id: number,
  formValues: StreamFormProps
): ThunkAction<void, ApplicationState, undefined, StreamEditAction> => async (
  dispatch,
  getState
) => {
  const { userId } = getState().auth;

  handleResponse(
    await backend.patch(`/streams/${id}`, formValues),
    dispatch,
    StreamActionTypes.Edit,
    StreamActionTypes.EditFailure,
    "Could not update stream",
    "/streams"
  );
};

export const fetchStream = (
  id: number
): ThunkAction<void, ApplicationState, undefined, StreamFetchAction> => async (
  dispatch,
  getState
) => {
  const { userId } = getState().auth;

  handleResponse(
    await backend.get(`/streams/${id}`),
    dispatch,
    StreamActionTypes.Fetch,
    StreamActionTypes.FetchFailure,
    "Could not update stream",
    null
  );
};

export const deleteStream = (
  id: number
): ThunkAction<void, ApplicationState, undefined, StreamDeleteAction> => async (
  dispatch,
  getState
) => {
  const { userId } = getState().auth;

  handleResponse(
    await backend.delete(`/streams/${id}`),
    dispatch,
    StreamActionTypes.Delete,
    StreamActionTypes.DeleteFailure,
    "Could not delete stream",
    "/streams",
    id
  );
};
