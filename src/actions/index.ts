import { AllAuthActions } from "./auth";
import { AllStreamActions } from "./streams";
import { ThunkAction } from "redux-thunk";
import { ApplicationState } from "../reducers";

export type ApplicationActions = AllAuthActions | AllStreamActions;
export type ThunkResult<R> = ThunkAction<
  R,
  ApplicationState,
  undefined,
  ApplicationActions
>;
