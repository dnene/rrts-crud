import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import reduxThunk, { ThunkMiddleware } from "redux-thunk";

import App from "./components/App";
import reducers, { ApplicationState } from "./reducers";
import { ApplicationActions } from "./actions";

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducers,
  composeEnhancers(
    applyMiddleware(
      reduxThunk as ThunkMiddleware<ApplicationState, ApplicationActions>
    )
  )
);

ReactDOM.render(
  <Provider store={store}>
    <App redirectTo={null} />
  </Provider>,
  document.querySelector("#root")
);
