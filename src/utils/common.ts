export interface IdMatchParams {
  id: string | undefined;
}
export function strToInt(idStr: string | undefined): number | null {
  if (idStr) {
    return parseInt(idStr);
  }
  return null;
}
